import axios from "axios";
import { useEffect, useState } from "react";

export const API_URL =
  "https://sleepy-citadel-66676-f7bd094e4a58.herokuapp.com";

const hosUurl = "https://sleepy-citadel-66676-f7bd094e4a58.herokuapp.com";

export const $api = axios.create({
  withCredentials: true,
  baseURL: API_URL,
});

$api.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

const useAxios = () => {
  const [res, setRes] = useState([]);
  const [err, setErr] = useState("");
  const [load, setLoad] = useState(true);
  // const [users, setUsers] = useState([]);
  // const [usersErr, setUsersErr] = useState("");
  // const [usersLoad, setUsersLoad] = useState(true);
  useEffect(() => {
    axios
      .get("https://dummyjson.com/products")
      .then((res) => setRes(res.data.products))
      .catch((err) => setErr(err))
      .finally(() => setLoad(false));
    // axios
    //   .get("/users")
    //   .then((res) => setUsers(res.data))
    //   .catch((err) => setUsersErr(err))
    //   .finally(() => setUsersLoad(false));
  }, []);
  // console.log(users);
  return { res, err, load };
};

export default useAxios;

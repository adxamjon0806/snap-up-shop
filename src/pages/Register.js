import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import UserFetch from "../Api/UserFetchApi";
import { useDispatch } from "react-redux";

const Register = () => {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [err, setErr] = useState(null);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  return (
    <section className="Login">
      <h1>Sign Up</h1>
      <input
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        placeholder="Enter your email"
      />
      <input
        value={password}
        type="password"
        onChange={(e) => setPassword(e.target.value)}
        placeholder="Enter password"
      />
      {err ? <p className="error__message">{err}</p> : ""}
      <button
        onClick={async () => {
          const err = await UserFetch.registration(email, password, dispatch);
          if (err) {
            setErr(err);
          } else {
            navigate("/");
          }
          try {
            const response = await UserFetch.registration(
              email,
              password,
              dispatch
            );
            navigate("/");
          } catch (e) {
            setErr(e.response?.data?.message);
          }
        }}
      >
        Sign Up
      </button>
    </section>
  );
};

export default Register;

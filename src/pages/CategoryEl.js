import React from "react";
import { useParams } from "react-router-dom";
import SeeProduct from "../components/SeeProduct";
import useAxios from "../hooks/useAxios";

const CategoryEl = () => {
  const { res } = useAxios();
  const { category } = useParams();
  const products = res.filter((product) => product.category === category);
  console.log(category);
  return (
    <section className="products">
      <SeeProduct props={[products, category.toUpperCase()]} />
    </section>
  );
};

export default CategoryEl;

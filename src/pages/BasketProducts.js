import React from "react";
import { useBasket } from "../Context/BasketContext";
import shopingNothing from "../images/shopping_cart.png";
import { Link } from "react-router-dom";
import BasketCart from "../components/BasketCart";

const BasketProducts = () => {
  const { basket } = useBasket();
  return (
    <section className="basketSec">
      {basket.length === 0 ? (
        <div className="nothing">
          <img width={150} src={shopingNothing} alt="shoping-cart" />
          <p>Your shopping cart is empty.</p>
          <Link to={"/"}>Go shopping Now</Link>
        </div>
      ) : (
        <>
          <div className="backetSec__NamingDiv">
            <p>S.N.</p>
            <p>Product</p>
            <p>Unit Price</p>
            <p>Quantity</p>
            <p>Total Price</p>
            <p>Actions</p>
          </div>
          {basket.map((product, i) => {
            return <BasketCart key={product.data.id} product={product} i={i} />;
          })}
        </>
      )}
    </section>
  );
};

export default BasketProducts;

import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import UserFetch from "../Api/UserFetchApi";
import { useDispatch } from "react-redux";

const Login = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState([]);
  const [password, setPassword] = useState([]);
  const [err, setErr] = useState(null);
  const dispatch = useDispatch();

  return (
    <section className="Login">
      <h1 className="">Log in</h1>
      <input
        onChange={(e) => setEmail(e.target.value)}
        value={email}
        className=""
        placeholder="Enter you email . . ."
      />
      <input
        value={password}
        type="password"
        onChange={(e) => setPassword(e.target.value)}
        placeholder="Enter password"
      />
      {err ? <p className="error__message">{err}</p> : ""}
      <button
        className=""
        onClick={async () => {
          try {
            const response = await UserFetch.login(email, password, dispatch);
            navigate("/");
          } catch (e) {
            setErr(e.response?.data?.message);
          }
        }}
      >
        Next
      </button>
    </section>
  );
};

export default Login;

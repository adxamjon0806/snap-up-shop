import React from "react";
import Cart from "./Cart";

const SeeProduct = ({ props }) => {
  const [product, naming] = props;
  return (
    <div>
      <div className="See__products">
        <p>{naming}</p>
      </div>
      <div className="carts">
        {product.map((product) => {
          return <Cart key={product.id} product={product} />;
        })}
      </div>
    </div>
  );
};

export default SeeProduct;

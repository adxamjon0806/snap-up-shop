import React, { useState } from "react";
import { useBasket } from "../Context/BasketContext";

const BasketCart = ({ product, i }) => {
  const [count, setCount] = useState(product.count);
  const { basket, setBasket } = useBasket();
  function DeleteBasketProduct() {
    const basketWithoutDelProduct = basket.filter(
      (basProd) => basProd.data.id !== product.data.id
    );
    setBasket(basketWithoutDelProduct);
  }
  return (
    <div key={product.data.id} className="backetSec__NamingDiv Not__Naming">
      <p>{i + 1}</p>
      <p className="title">{product.data.title}</p>
      <p>
        $
        {(
          product.data.price -
          (product.data.price * product.data.discountPercentage) / 100
        ).toFixed(2)}
      </p>
      <div className="counter">
        <button
          onClick={() =>
            count > 1 ? setCount((prew) => prew - 1) : product.count
          }
        >
          -
        </button>
        {count}
        <button onClick={() => setCount((prew) => prew + 1)}>+</button>
      </div>
      <p className="TotalPrice">
        $
        {(
          product.data.price -
          (product.data.price * product.data.discountPercentage) / 100
        ).toFixed(2) * count}
      </p>
      <button onClick={DeleteBasketProduct}>Delete</button>
    </div>
  );
};

export default BasketCart;

import React from "react";
import useAxios from "../hooks/useAxios";
import SeeProduct from "./SeeProduct";

const Products = () => {
  const { res } = useAxios();
  const Smartphones = "SMARTPHONES";
  const SeeProducts = "SEE OUR PRODUCTS";
  const Laptops = "LAPTOPS";
  const Frangrances = "FRANGRANCES";

  const frangrances = res.filter(
    (product) => product.category === "fragrances"
  );
  const laptops = res.filter((product) => product.category === "laptops");
  const smartphones = res.filter(
    (product) => product.category === "smartphones"
  );
  return (
    <section className="products">
      <SeeProduct props={[res, SeeProducts]} />
      <SeeProduct props={[smartphones, Smartphones]} />
      <SeeProduct props={[laptops, Laptops]} />
      <SeeProduct props={[frangrances, Frangrances]} />
    </section>
  );
};

export default Products;

import React from "react";
import { Link } from "react-router-dom";

const Cart = ({ product }) => {
  // console.log(product);
  let { brand, category, discountPercentage, id, images, price, title } =
    product;
  return (
    <Link className="cart" to={`/products/${id}`}>
      <div className="category">{category}</div>
      <img src={images[0]} alt="product" width={300} />
      <div className="cart__desc">
        <p className="desc__brand">
          Brand: <b>{brand}</b>
        </p>
        <p>{title}</p>
        <div className="desc__prices">
          <p className="old__price">{price.toFixed(2)}</p>
          <b className="new__price">
            ${(price - (price * discountPercentage) / 100).toFixed(2)}
          </b>
          <p className="off">(% off)</p>
        </div>
        <div className="redLine"></div>
      </div>
    </Link>
  );
};

export default Cart;

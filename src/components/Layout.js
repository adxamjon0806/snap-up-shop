import React, { useState } from "react";
import { Link, Outlet } from "react-router-dom";
import { useBasket } from "../Context/BasketContext";
import { useDispatch, useSelector } from "react-redux";
import UserFetch from "../Api/UserFetchApi";

const Layout = () => {
  const [active, setActive] = useState(false);
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const { basket } = useBasket();
  return (
    <div>
      <header className="header">
        <nav className="nav">
          <div className="nav__top">
            <div className="nav__top__div">
              <p className="top__div__p">Seller Center</p>
              <button
                className="top__div__btn"
                onClick={() => UserFetch.logout(dispatch)}
              >
                Log out
              </button>
              <p className="top__div__p">
                {user.email ? user.email : "Follow us on"}
              </p>
            </div>
            <div className="nav__top__div">
              <Link to={"/login"} className="top__div__p">
                Admin
              </Link>
              <Link to={"register"} className="top__div__p">
                Sign up
              </Link>
              <Link to={"login"} className="top__div__p">
                Log in
              </Link>
            </div>
          </div>
          <div className="nav__bottom">
            <Link to={"/"} className="nav__bottom__logo">
              <b>Snap</b>Up
            </Link>
            <div className="nav__bottom__int">
              <div className="bottom__bas__inp">
                <input
                  type="text"
                  className="nav__bottom__input"
                  placeholder="Search your preffered items here"
                />
                <Link to={"/basket"} className="button">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className="bi bi-cart3"
                    viewBox="0 0 16 16"
                  >
                    <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                  </svg>
                  <div className="basket">{basket.length}</div>
                </Link>
              </div>
              <div
                className={`nav__bottom__categories ${active ? "active" : ""}`}
              >
                <Link
                  to={"/category/smartphones"}
                  onClick={() => setActive(!active)}
                >
                  Smartphones
                </Link>
                <Link
                  to={"/category/laptops"}
                  onClick={() => setActive(!active)}
                >
                  Laptops
                </Link>
                <Link
                  to={"/category/fragrances"}
                  onClick={() => setActive(!active)}
                >
                  Frangrances
                </Link>
                <Link
                  to={"/category/skincare"}
                  onClick={() => setActive(!active)}
                >
                  Skincare
                </Link>
                <Link
                  to={"/category/groceries"}
                  onClick={() => setActive(!active)}
                >
                  Groseries
                </Link>
                <Link
                  to={"/category/home-decoration"}
                  onClick={() => setActive(!active)}
                >
                  Home Decoration
                </Link>
              </div>
            </div>
            <button
              className={`menuBtn ${active ? "activeBtn" : ""}`}
              onClick={() => {
                setActive(!active);
              }}
            >
              <div></div>
            </button>
          </div>
        </nav>
      </header>
      <Outlet />
      <footer>
        <div className="div__top">
          <p>PRIVACY POLICY</p>
          <p className="center">TERM OF SERVICE</p>
          <p>ABOUT SNAPUP.</p>
        </div>
        <div className="div__bottom">© 2022 SnapUp. All Rights Reserved.</div>
      </footer>
    </div>
  );
};

export default Layout;

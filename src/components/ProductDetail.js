import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import useAxios from "../hooks/useAxios";
import { useBasket } from "../Context/BasketContext";

const ProductDetail = () => {
  const { basket, setBasket } = useBasket();
  const [counter, setCounter] = useState(1);
  const [data, setData] = useState({});
  const { res, load } = useAxios();
  const { idOfParam } = useParams();
  const productWithId = res.filter(
    (product) => product.id === Number(idOfParam)
  );
  function AddToCart() {
    const filteredBasket = basket.filter(
      (product) => product.data.id === data.id
    );
    if (filteredBasket.length > 0) {
      const newBasket = basket.filter((product) => product.data.id !== data.id);
      newBasket.push({ data, count: counter + filteredBasket[0].count });
      setBasket(newBasket);
    } else {
      setBasket([...basket, { data, count: counter }]);
    }
  }
  useEffect(() => {
    setData(...productWithId);
  }, [productWithId]);
  return (
    <section className="product__detail">
      {load ? (
        <h1>Loading . . .</h1>
      ) : (
        <div className="product">
          <div className="product__left">
            <img
              src={data ? (data.images ? data.images[0] : "") : ""}
              alt="product__main"
              className="main__img"
            />
            <div className="left__bottom">
              {data
                ? data.images
                  ? data.images.map((img, index) => {
                      if (index < 4) {
                        return (
                          <div key={img} className="img__list">
                            <img
                              src={img}
                              alt="product"
                              className="bottom__img"
                            />
                          </div>
                        );
                      }
                      return "";
                    })
                  : undefined
                : undefined}
            </div>
          </div>
          <div className="product__rihgt">
            <h2>{data?.title}</h2>
            <p className="right__desc">{data?.description}</p>
            <div className="RBC">
              <p>
                <b>Rating: </b>
                {data?.rating}
              </p>
              <p className="brand">
                <b>Brand: </b>
                {data?.brand}
              </p>
              <p>
                <b>Category: </b>
                {data?.category}
              </p>
            </div>
            <div className="pricment">
              <p className="price">
                <b>${data?.price?.toFixed(2)}</b>
                Inclusive of all taxes
              </p>
              <p className="percented__Price">
                $
                {(
                  data?.price -
                  (data?.price * data?.discountPercentage) / 100
                ).toFixed(2)}
                <span>{data?.discountPercentage}% Off</span>
              </p>
            </div>
            <div className="quantity">
              Quantity:
              <div className="counter">
                <button
                  onClick={() =>
                    counter > 1 ? setCounter((prew) => prew - 1) : counter
                  }
                >
                  -
                </button>
                {counter}
                <button onClick={() => setCounter((prew) => prew + 1)}>
                  +
                </button>
              </div>
            </div>
            <div className="btns">
              <button className="Add" onClick={AddToCart}>
                Add To Cart
              </button>
              <button className="Buy">Buy Now</button>
            </div>
          </div>
        </div>
      )}
    </section>
  );
};

export default ProductDetail;

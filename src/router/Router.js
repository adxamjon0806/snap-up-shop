import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Products from "../components/Products";
import ProductDetail from "../components/ProductDetail";
import Login from "../pages/Login";
import Register from "../pages/Register";
import CategoryEl from "../pages/CategoryEl";
import BasketProducts from "../pages/BasketProducts";
import Layout from "../components/Layout";

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Products />} />
          <Route path={"products"}>
            <Route index element={<Products />} />
            <Route path=":idOfParam" element={<ProductDetail />} />
          </Route>
          <Route path={"category"}>
            <Route path=":category" element={<CategoryEl />} />
          </Route>
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="/basket" element={<BasketProducts />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default Router;

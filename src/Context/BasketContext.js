import React, { createContext, useContext, useState } from "react";

const Context = createContext();

const BasketProvider = ({ children }) => {
  const [basket, setBasket] = useState([]);
  return (
    <Context.Provider value={{ basket, setBasket }}>
      {children}
    </Context.Provider>
  );
};

function useBasket() {
  return useContext(Context);
}

export { BasketProvider, useBasket };

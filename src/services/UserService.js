import { $api } from "../hooks/useAxios";

export default class UserService {
  static fetchUsers() {
    return $api.get("/users");
  }
}

import { setAuth } from "../redux/Reducers/AuthSlice";
import { setUser } from "../redux/Reducers/UserSlice";
import AuthService from "../services/AuthService";
import { $api } from "../hooks/useAxios";

export default class UserFetch {
  static async login(email, password, dispatch) {
    try {
      const response = await AuthService.login(email, password);
      localStorage.setItem(
        "AccessToken",
        JSON.stringify(response.data.accessToken)
      );
      localStorage.setItem(
        "RefreshToken",
        JSON.stringify(response.data.refreshToken)
      );
      console.log(response.data);
      dispatch(setAuth(true));
      dispatch(setUser(response.data.user));
    } catch (e) {
      return e.response?.data?.message;
    }
  }
  static async registration(email, password, dispatch) {
    try {
      const response = await AuthService.registration(email, password);
      localStorage.setItem(
        "AccessToken",
        JSON.stringify(response.data.accessToken)
      );
      localStorage.setItem(
        "RefreshToken",
        JSON.stringify(response.data.refreshToken)
      );
      dispatch(setAuth(true));
      dispatch(setUser(response.data.user));
    } catch (e) {
      return e.response?.data?.message;
    }
  }
  static async logout(dispatch) {
    try {
      const refreshToken = JSON.parse(localStorage.getItem("RefreshToken"));
      const response = await AuthService.logout(refreshToken);
      localStorage.removeItem("AccessToken");
      localStorage.removeItem("RefreshToken");
      dispatch(setAuth(false));
      dispatch(setUser({}));
    } catch (e) {
      console.log(e.response?.data?.message);
    }
  }
  static async checkAuth(dispatch) {
    try {
      const refreshToken = JSON.parse(localStorage.getItem("RefreshToken"));
      const response = await $api.post(`/refresh`, { refreshToken });
      localStorage.setItem(
        "AccessToken",
        JSON.stringify(response.data.accessToken)
      );
      localStorage.setItem(
        "RefreshToken",
        JSON.stringify(response.data.refreshToken)
      );
      dispatch(setAuth(true));
      dispatch(setUser(response.data.user));
    } catch (e) {
      console.log(e.response?.data?.message);
    }
  }
}
